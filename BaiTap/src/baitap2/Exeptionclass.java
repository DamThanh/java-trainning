package baitap2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileReader;

/**
 * class de nem ra cac ngoai le trong cac truong hop dien hinh
 *
 * @author Dam Tien Thanh
 */
public class Exeptionclass {

    /**
     * ham khoi tao doi tuong lop nem ra ngoai le rong
     */
    public Exeptionclass() {

    }

    /**
     * ham nem ra ngoai le la con tro null neu loi do xay ra
     *
     * @param s chuoi ki tu null duong truyen vao
     * @return chuoi ki tu duoc viet thuong
     * @throws NullPointerException
     */
    public String testNullPointer(String s) throws NullPointerException {
        return s.toLowerCase();
    }

    /**
     * ham thu nem ra ngoai le chia cho 0 neu phep tinh chia cho 0
     *
     * @param num1 so bi chia
     * @param num2 so chia
     * @return thuong cua 2 so
     * @throws ArithmeticException
     */
    public int testDivide(int num1, int num2) throws ArithmeticException {
        return num1 / num2;
    }

    /**
     * ham thu nem ra ngoai le qua do dai cua mang neu loi xay ra
     *
     * @param index chi so truyen vao
     * @return phan tu ơ vi tri co chi so truyen vao cua mang
     * @throws ArrayIndexOutOfBoundsException
     */
    public int testArrayError(int index) throws ArrayIndexOutOfBoundsException {
        int[] arr = new int[2];
        return arr[index];
    }

    /**
     * ham thu nem ra ngoai le ep kieu neu loi xay ra
     *
     * @return gia tri duoc ep kieu
     * @throws ClassCastException
     */
    public int testClassCast() throws ClassCastException {
        Object obj = new Object();
        int a = (int) obj;
        return a;
    }

    /**
     * ham thu nem ra ngoai le khong tim thay file hoac loi doc/ghi thong tin ra
     * file neu loi do xay ra
     *
     * @param name ten cua file can doc
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void testFileNotExist(String name) throws FileNotFoundException, IOException {
        Scanner file = new Scanner(new File(name));
        System.out.println(file.nextInt());
    }
}
