package baitap.edu;

/**
 * class de thuc hien binh phuong bieu thuc
 *
 * @author Dam Tien Thanh
 */
public class Square extends Expression {

    //bieu thuc can duoc binh phuong
    private Expression expression;

    /**
     * khoi tao doi tuong bieu thuc co du lieu truyen vao
     *
     * @param expression
     */
    public Square(Expression expression) {
        this.expression = expression;
    }

    /**
     * lay ra gia tri cua bieu thuc can duoc binh phuong
     *
     * @return gia tri cua bieu thuc dang can duoc binh phuong
     */
    public Expression getExpression() {
        return expression;
    }

    /**
     * truyen vao bieu thuc dang can duoc binh phuong
     *
     * @param expression bieu thuc dang can duoc binh phuong
     */
    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    /**
     * binh phuong bieu thuc
     *
     * @return gia tri bieu thuc sau khi duoc binh phuong
     */
    public int evaluate() {
        int value = expression.evaluate();
        return value * value;
    }

    /**
     * lay ra thong tin cua bieu thuc sau khi binh phuong
     *
     * @return bieu thuc sau khi binh phuong
     */
    public String toString() {
        return "bieu thuc sau khi binh phuong la:" + evaluate();
    }
}
