package baitap.edu;

/**
 * class thuc hien phep cong cho 2 bieu thuc
 *
 * @author Dam Tien Thanh
 */
public class Addition extends BinaryExpression {

    //bieu thuc ben trai phep cong
    private Expression left;
    //bieu thuc ben phai phep cong
    private Expression right;

    /**
     * ham khoi tao doi tuong phep cong voi hai bieu thuc duoc truyen vao de
     * thuc hien phep cong
     *
     * @param left bieu thuc ben trai phep cong
     * @param right bieu thuc ben phai phep cong
     */
    public Addition(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    /**
     * lay ra bieu thuc ben trai phep cong
     *
     * @return bieu thuc ben trai phep cong
     */
    public Expression getLeft() {
        return left;
    }

    /**
     * truyen vao bieu thuc ben trai phep cong
     *
     * @param left bieu thuc ben trai phep cong
     */
    public void setLeft(Expression left) {
        this.left = left;
    }

    /**
     * lay ra bieu thuc ben phai phep cong
     *
     * @return bieu thuc ben phai phep cong
     */
    public Expression getRight() {
        return right;
    }

    /**
     * truyen vao bieu thuc ben phai phep cong
     *
     * @param right bieu thuc ben phai phep cong
     */
    public void setRight(Expression right) {
        this.right = right;
    }

    /**
     * ham tra ve bieu thuc ben trai
     *
     * @return bieu thuc ben trai
     */
    public Expression left() {
        return left;
    }

    /**
     * ham tra ve bieu thuc ben phai
     *
     * @return bieu thuc ben phai
     */
    public Expression right() {
        return right;
    }

    /**
     * tinh tong hai bieu thuc
     *
     * @return tong hai bieu thuc
     */
    public int evaluate() {
        //gia tri bieu thuc ben trai
        int value1 = left.evaluate();
        //gia tri bieu thuc ben phai
        int value2 = right.evaluate();
        return value1 + value2;
    }

    /**
     * tra ve thong tin tong hai bieu thuc
     *
     * @return tong hai bieu thuc
     */
    public String toString() {
        return "Tong hai bieu thuc la: " + evaluate();
    }
}
