package baitap.edu;

/**
 * class dai dien cho 2 ve cua mot bieu thuc nhi phan
 *
 * @author Dam Tien Thanh
 */
public abstract class BinaryExpression extends Expression {

    /**
     * ham tra ve bieu thuc phia ben trai
     *
     * @return bieu thuc phia ben trai
     */
    public abstract Expression left();

    /**
     * ham tra ve bieu thuc phia ben phai
     *
     * @return bieu thuc phia ben phai
     */
    public abstract Expression right();
}
