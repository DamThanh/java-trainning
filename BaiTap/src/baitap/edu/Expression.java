package baitap.edu;

/**
 * class dai dien cho bieu thuc toan hoc
 *
 * @author Dam Tien Thanh
 */
public abstract class Expression {

    /**
     * ham lay ra thong tin cua bieu thuc
     *
     * @return thong tin cua bieu thuc
     */
    public abstract String toString();

    /**
     * ham tinh gia tri cua bieu thuc
     *
     * @return gia tri cua bieu thuc
     */
    public abstract int evaluate();
}
