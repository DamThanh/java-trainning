package baitap.edu;

/**
 * class de chay chuong trinh
 *
 * @author Dam Tien Thanh
 */
public class ExpressionTest {
    public static void main(String[] args) {
        Expression a=new Numeral(10);
        Expression b=new Numeral(1);
        Expression c=new Numeral(2);
        Expression d=new Numeral(3);
        Expression e=new Square(a);
        Expression f=new Subtraction(e, b);
        Expression g=new Multiplication(c, d);
        Expression h=new Addition(f, g);
        Expression i=new Square(h);
        System.out.println(i.toString());
        try {
            Expression aa=new Numeral(0);
            Expression bb=new Numeral(1);
            Expression cc=new Division(bb,aa);
            cc.evaluate();
        } catch (java.lang.ArithmeticException q) {
            System.out.println("Loi chia cho 0");
        }
    }
}
