package baitap.edu;

/**
 * class dai dien cho so trong bieu thuc
 *
 * @author Dam Tien Thanh
 */
public class Numeral extends Expression {

    //tham so cua bieu thuc
    private int value;

    /**
     * ham khoi tao doi tuong tham so rong
     */
    public Numeral() {
    }

    /**
     * ham khoi tao tham so co du lieu truyen vao
     *
     * @param value tham so truyen vao
     */
    public Numeral(int value) {
        this.value = value;
    }

    /**
     * lay ra gia tri tham so
     *
     * @return gia tri tham so
     */
    public int getValue() {
        return value;
    }

    /**
     * truyen vao gia tri tham so
     *
     * @param value gia tri tham so
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * lay ra thong tin cua tham so
     *
     * @return thon tin cua tham so
     */
    public String toString() {
        return "Tham so cua bieu thuc la:" + getValue();
    }

    /**
     * tra ve gia tri tham so
     *
     * @return gia tri tham so
     */
    public int evaluate() {
        return value;
    }
}
