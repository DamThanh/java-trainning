package baitap.edu;

/**
 * class de thuc hien phep nhan toan hoc
 *
 * @author Dam Tien Thanh
 */
public class Multiplication extends BinaryExpression {

    //bieu thuc ben trai phep nhan
    private Expression left;
    //bieu thuc ben phai phep nhan
    private Expression right;

    /**
     * ham khoi tao doi tuong phep cong voi hai bieu thuc duoc truyen vao de
     * thuc hien phep nhan
     *
     * @param left bieu thuc ben trai phep nhan
     * @param right bieu thuc ben phai phep nhan
     */
    public Multiplication(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    /**
     * lay ra bieu thuc ben trai phep nhan
     *
     * @return bieu thuc ben trai phep nhan
     */
    public Expression getLeft() {
        return left;
    }

    /**
     * truyen vao bieu thuc ben trai phep nhan
     *
     * @param left bieu thuc ben trai phep nhan
     */
    public void setLeft(Expression left) {
        this.left = left;
    }

    /**
     * lay ra bieu thuc ben phai phep nhan
     *
     * @return bieu thuc ben phai phep nhan
     */
    public Expression getRight() {
        return right;
    }

    /**
     * truyen vao bieu thuc ben phai phep nhan
     *
     * @param right bieu thuc ben phai phep nhan
     */
    public void setRight(Expression right) {
        this.right = right;
    }

    /**
     * ham tra ve bieu thuc ben trai
     *
     * @return bieu thuc ben trai
     */
    public Expression left() {
        return left;
    }

    /**
     * ham tra ve bieu thuc ben phai
     *
     * @return bieu thuc ben phai
     */
    public Expression right() {
        return right;
    }

    /**
     * tinh tich hai bieu thuc
     *
     * @return tich hai bieu thuc
     */
    public int evaluate() {
        //gia tri bieu thuc ben trai
        int value1 = left.evaluate();
        //gia tri bieu thuc ben phai
        int value2 = right.evaluate();
        return value1 * value2;
    }

    /**
     * tra ve thong tin tich hai bieu thuc
     *
     * @return tich hai bieu thuc
     */
    public String toString() {
        return "Tich hai bieu thuc la: " + evaluate();
    }
}
