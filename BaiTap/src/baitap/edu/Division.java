package baitap.edu;

/**
 * class de thuc hien phep chia toan hoc
 *
 * @author Dam Tien Thanh
 */
public class Division extends BinaryExpression {

    //bieu thuc ben trai phep cong
    private Expression left;
    //bieu thuc ben phai phep cong
    private Expression right;

    /**
     * ham khoi tao doi tuong phep cong voi hai bieu thuc duoc truyen vao de
     * thuc hien phep chia
     *
     * @param left bieu thuc ben trai phep chia
     * @param right bieu thuc ben phai phep chia
     */
    public Division(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    /**
     * lay ra bieu thuc ben trai phep chia
     *
     * @return bieu thuc ben trai phep chia
     */
    public Expression getLeft() {
        return left;
    }

    /**
     * truyen vao bieu thuc ben trai phep chia
     *
     * @param left bieu thuc ben trai phep chia
     */
    public void setLeft(Expression left) {
        this.left = left;
    }

    /**
     * lay ra bieu thuc ben phai phep chia
     *
     * @return bieu thuc ben phai phep chia
     */
    public Expression getRight() {
        return right;
    }

    /**
     * truyen vao bieu thuc ben phai phep chia
     *
     * @param right bieu thuc ben phai phep chia
     */
    public void setRight(Expression right) {
        this.right = right;
    }

    /**
     * ham tra ve bieu thuc ben trai
     *
     * @return bieu thuc ben trai
     */
    public Expression left() {
        return left;
    }

    /**
     * ham tra ve bieu thuc ben phai
     *
     * @return bieu thuc ben phai
     */
    public Expression right() {
        return right;
    }

    /**
     * tinh chia hai bieu thuc
     *
     * @return thuong cua hai bieu thuc
     */
    public int evaluate() {
        //gia tri bieu thuc ben trai
        int value1 = left.evaluate();
        //gia tri bieu thuc ben phai
        int value2 = right.evaluate();
        return value1 / value2;
    }

    /**
     * tra ve thong tin thuong hai bieu thuc
     *
     * @return thuong hai bieu thuc
     */
    public String toString() {
        return "Hai bieu thuc chia cho nhau duoc: " + evaluate();
    }
}
