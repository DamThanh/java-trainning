package baitap.edu;

/**
 * class de thuc hien phep tru hai bieu thuc toan hoc
 *
 * @author Dam Tien Thanh
 */
public class Subtraction extends BinaryExpression{
    //bieu thuc ben trai phep tru

    private Expression left;
    //bieu thuc ben phai phep tru
    private Expression right;

    /**
     * ham khoi tao doi tuong phep tru voi hai bieu thuc duoc truyen vao de thuc
     * hien phep tru
     *
     * @param left bieu thuc ben trai phep tru
     * @param right bieu thuc ben phai phep tru
     */
    public Subtraction(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    /**
     * lay ra bieu thuc ben trai phep tru
     *
     * @return bieu thuc ben trai phep tru
     */
    public Expression getLeft() {
        return left;
    }

    /**
     * truyen vao bieu thuc ben trai phep tru
     *
     * @param left bieu thuc ben trai phep tru
     */
    public void setLeft(Expression left) {
        this.left = left;
    }

    /**
     * lay ra bieu thuc ben phai phep tru
     *
     * @return bieu thuc ben phai phep tru
     */
    public Expression getRight() {
        return right;
    }

    /**
     * truyen vao bieu thuc ben phai phep tru
     *
     * @param right bieu thuc ben phai phep tru
     */
    public void setRight(Expression right) {
        this.right = right;
    }

    /**
     * ham tra ve bieu thuc ben trai
     *
     * @return bieu thuc ben trai
     */
    public Expression left() {
        return left;
    }

    /**
     * ham tra ve bieu thuc ben phai
     *
     * @return bieu thuc ben phai
     */
    public Expression right() {
        return right;
    }

    /**
     * tinh hieu hai bieu thuc
     *
     * @return hieu hai bieu thuc
     */
    public int evaluate() {
        //gia tri bieu thuc ben trai
        int value1 = left.evaluate();
        //gia tri bieu thuc ben phai
        int value2 = right.evaluate();
        return value1 - value2;
    }

    /**
     * tra ve thong tin hieu hai bieu thuc
     *
     * @return hieu hai bieu thuc
     */
    public String toString() {
        return "Hieu hai bieu thuc la: " + evaluate();
    }
}
