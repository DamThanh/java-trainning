package Bai2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dam Thanh
 */
public class FindWay {

    private static String matrix[][] = new String[10][10];

    public static void readFile(String fileName) {
        try {
            FileInputStream file = new FileInputStream(fileName);
            Scanner sc = new Scanner(file);
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    matrix[i][j] = new String(sc.next());
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FindWay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void findWayOut(int i, int j) {

        /*if ((j + 1 >= 5 && i + 1 >= 5) || (!matrix[i + 1][j].contains("P") && !matrix[i][j + 1].contains("P"))) {
            return;
        }*/
        if (matrix[i][j].contains("E")) {
            System.out.println("E(" + i + "," + j + ")");
            return;
        }
        System.out.print("S(" + i + "," + j + ")"+"->");
       
        if (i + 1 < 5 && !matrix[i + 1][j].contains("T")) {
            findWayOut(i + 1, j);
        }else if (j + 1 < 5 && !matrix[i][j + 1].contains("T")) {
            findWayOut(i, j + 1);
        }
    }

    public static void main(String[] args) {
        readFile("map.txt");
        //if(matrix[0][0].contains("S")) System.out.println("right");
        findWayOut(0, 0);
    }
}
