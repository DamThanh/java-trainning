package Bai1;

import java.util.Scanner;

/**
 *
 * @author Dam Thanh
 */
public class GoodString {

    public static int findGoodString(String s) {
        int max = 0, j = 0;
        s.toLowerCase();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i' || s.charAt(i) == 'o' || s.charAt(i) == 'u') {
                j++;
            }else {
                if (max < j) {
                    max = j;
                }
                j = 0;
            }
            if(i==s.length()-1 && max<j) {
                max=j;
                j=0;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s;
        System.out.println("Nhập vào một xâu: ");
        s = sc.nextLine();
        int result = findGoodString(s);
        if(result==0){
            System.out.println("Không tồn tại xâu đẹp");
            return;
        }
        System.out.println("Xâu đẹp lớn nhất có độ dài là: " + result);
    }
}
